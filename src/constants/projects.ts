// const projects = [
//   {
//     key: 1,
//     title: "React E-Commerce",
//     imageSrc:
//       "https://cdn.pixabay.com/photo/2018/02/04/17/39/crypto-currency-3130381_640.jpg",
//     description: "This is a project made with complete React js.",
//     demo: "https://www.example.com",
//     source: "https://www.github.com",
//   },
//   {
//     key: 2,
//     title: "MERN Blogging",
//     imageSrc:
//       "https://cdn.pixabay.com/photo/2017/05/02/15/30/streets-2278471_640.jpg",
//     description: "This is a project made with complete React js.",
//     demo: "https://www.example.com",
//     source: "https://www.github.com",
//   },
//   {
//     key: 3,
//     title: "React Recipe",
//     imageSrc:
//       "https://cdn.pixabay.com/photo/2016/08/16/17/12/skyscrapers-1598418_640.jpg",
//     description: "This is a project made with complete React js.",
//     demo: "https://www.example.com",
//     source: "https://www.github.com",
//   },
//   {
//     key: 4,
//     title: "React Firebase App",
//     imageSrc:
//       "https://cdn.pixabay.com/photo/2021/03/02/01/07/cyberpunk-6061251_640.jpg",
//     description: "This is a project made with complete React js.",
//     demo: "https://www.example.com",
//     source: "https://www.github.com",
//   },
// ];

// export default projects;

const projects = [
  {
    key: 1,
    title: "Real-Time Chat & Blogging",
    imageSrc:
      "https://cdn.pixabay.com/photo/2018/02/04/17/39/crypto-currency-3130381_640.jpg",
    description:
      "A real-time chat application built using React.js and Socket.io for seamless communication.",
    demo: "https://www.example.com",
    source: "https://www.github.com",
  },
  {
    key: 2,
    title: "E-Commerce Application",
    imageSrc:
      "https://cdn.pixabay.com/photo/2017/05/02/15/30/streets-2278471_640.jpg",
    description:
      "An e-commerce website for garments created with MERN Stack...",
    demo: "https://www.example.com",
    source: "https://www.github.com",
  },
  {
    key: 3,
    title: "TechCrunch Clone",
    imageSrc:
      "https://cdn.pixabay.com/photo/2016/08/16/17/12/skyscrapers-1598418_640.jpg",
    description:
      "A clone of TechCrunch developed in order to mimic its design and functionality.",
    demo: "https://www.example.com",
    source: "https://www.github.com",
  },
];

export default projects;
