const skills = [
  {
    title: "HTML",
    imageSrc: "skills/html.png",
  },
  {
    title: "CSS",
    imageSrc: "skills/css.png",
  },
  {
    title: "BootStrap",
    imageSrc: "skills/bootstrap1.jpeg",
  },
  {
    title: "JavaScript",
    imageSrc: "skills/javascript.png",
  },
  {
    title: "TypeScript",
    imageSrc: "skills/typescript.png",
  },
  {
    title: "ReactJS",
    imageSrc: "skills/react.png",
  },
  {
    title: "Andt Design",
    imageSrc: "skills/andt.jpeg",
  },
  {
    title: "NodeJS",
    imageSrc: "skills/node.png",
  },
  {
    title: "Version  Control",
    imageSrc: "skills/firebase.png",
  },
  {
    title: "MongoDB",
    imageSrc: "skills/mongodb.png",
  },
  
];

export default skills;
