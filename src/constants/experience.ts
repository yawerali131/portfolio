// const experienceData = [
//   {
//     id: 1,
//     role: "Senior Software Development Engineer SDE - II",
//     organisation: "Google",
//     startDate: "Sept, 2023",
//     endDate: "Present",
//     location: "CA, United States",
//     experiences: ["Worked on Google Gmail", "Worked with the Google Bard Team"],
//     imageSrc: "company/google.png",
//   },
//   {
//     id: 2,
//     role: "Software Development Engineer SDE - I",
//     organisation: "Microsoft",
//     startDate: "Aug, 2022",
//     endDate: "Aug, 2023",
//     location: "London, UK",
//     experiences: [
//       "Worked on Windows 10 terminal",
//       "Worked on Window 11 Upcoming features",
//     ],
//     imageSrc: "company/microsoft.png",
//   },
//   {
//     id: 3,
//     role: "Software Development Engineer - Intern",
//     organisation: "Netflix",
//     startDate: "Apr, 2021",
//     endDate: "Jun, 2022",
//     location: "Bengaluru, India",
//     experiences: [
//       "Worked on Netflix payment system",
//       "Helped to create UI for video player",
//     ],
//     imageSrc: "company/netflix.png",
//   },
// ];

// export default experienceData;

const experienceData = [
  {
    id: 1,
    role: "Frontend Developer (Assistant Software Engineer)",
    organisation: "City Heart",
    startDate: "06/2022",
    endDate: "Present",
    location: "Kharar, Punjab, India",
    experiences: [
      "Managed and implemented frontend functionality using technologies like HTML, CSS.",
      "Leveraged JavaScript and ReactJS to enhance user interactions and experiences.",
      "Orchestrated API integrations for efficient data handling.",
      "Collaborated closely with cross-functional teams to fulfill functional requirements, delivering tailored frontend solutions.",
    ],
    imageSrc: "company/wil.png",
  },
  {
    id: 2,
    role: "UI Developer (Internship)",
    organisation: "InfoCampus Logics Pvt Ltd",
    startDate: "12/2021",
    endDate: "05/2023",
    location: "Bangalore, India",
    experiences: [
      "Worked with HTML, CSS, Bootstrap, JavaScript, ReactJS, JSON APIs, web services, and JSON-server.",
      "Designed layouts, integrated APIs, ensured browser compatibility, and addressed bugs.",
    ],
    imageSrc: "company/infoCam.png",
  },
];

export default experienceData;
