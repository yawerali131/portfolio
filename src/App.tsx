import { useEffect } from "react";
import * as Aos from "aos";
import "aos/dist/aos.css";
import NavBar from "./Views/NavBar";
import Experience from "./Views/Experience";
import Contact from "./Views/Contact";
import Projects from "./Views/Projects";
import Skills from "./Views/Skills";
import Home from "./Views/Home";

const App = () => {
  useEffect(() => {
    Aos.init();
  }, []);
  

  return (
    <>
      <NavBar />
      <div className="container">
        <Home />
        <Experience />
        <Skills />
        <Projects />
        <Contact />
      </div>
    </>
  );
};

export default App;
