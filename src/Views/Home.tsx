import { useEffect, useRef } from "react";
import hero from "../constants/hero";
import { Typed } from "react-typed";

const Home = () => {
  const typedRef = useRef(null);
  useEffect(() => {
    const options = {
      strings: [
        "Welcome To My Profile...",
        "My Name is Yawer Ali.",
        "I am a MERN Stack Developer.",
        "With over 2 Years of Experience,",
        "I have Worked Extensively with Frontend Technologies",
      ],
      typeSpeed: 50,
      backSpeed: 50,
      loop: true,
    };

    const typed = new Typed(typedRef.current, options);

    return () => {
      typed.destroy();
    };
  }, []);

  return (
    <>
      <div className="container home" id="home">
        <div className="left" data-aos="fade-up-right" data-aos-duration="1000">
          <h1 ref={typedRef}></h1>

          <a
            href={
              "https://drive.google.com/file/d/1e98qQkjC4LNKJ3ZKzWWKpyaFzaEiLnN_/view?usp=sharing"
            }
            download="yawer'sResume.pdf"
            className="btn btn-outline-warning my-3"
          >
            Download Resume
          </a>
        </div>
        <div className="right">
          <div className="img" data-aos="fade-up-left" data-aos-duration="1000">
            <img src={`/assets/${hero.imgSrc}`} alt="hero" />
          </div>
        </div>
      </div>
    </>
  );
};

export default Home;
